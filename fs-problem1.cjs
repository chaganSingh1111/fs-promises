
const fs = require("fs")
const path = require("path")

const problem1Fn = (directoryPath, numFiles) => {
  new Promise ((resolve,reject)=>{
    fs.mkdir(directoryPath, (err) => {
      if (err) {
        reject(err)
      }else{
        resolve()
      }
    })
  }).then(()=>{
    return new Promise((resolve,reject)=>{
          let countArr = []
          for (let idx = 1; idx <= numFiles; idx++) {
            const filename = `file${idx}.json`
            const filePath = path.join(directoryPath, filename)
            const randomData = { value: Math.random() }

            fs.writeFile(filePath, JSON.stringify(randomData), (err) => {
              if (err) {
                //console.error(`Error creating ${filename}:`, err)
              reject(err)
              }
              console.log(`Created ${filename}`)
              countArr.push(filename)
              if (countArr.length === numFiles) {
                resolve()
                // deleteFiles(directoryPath, numFiles)
              }
            })
          }
    })
          
  }).then(()=>{
    deleteFiles(directoryPath,numFiles)
  }).catch((err)=>{
    console.log(err)
  })
    
}

const deleteFiles = (directoryPath, numFiles) => {
  for (let idx = 1; idx <= numFiles; idx++) {
    const filename = `file${idx}.json`
    const filePath = path.join(directoryPath, filename)

    fs.unlink(filePath, (err) => {
      if (err) {
        console.error(`Error deleting ${filename}:`, err)
        return
      }
      console.log(`Deleted ${filename}`)
    })
  }
}

module.exports = problem1Fn

