const fs = require('fs')
const path = require('path')


// Read the given file lipsum.txt
const tempFile =(lipsum)=>{

 const readile =(file)=>{
  return new Promise((resolve,reject)=>{

    let filepath = path.join(__dirname,file)
    fs.readFile(filepath, 'utf8', (err, data) => {
      if (err) {
        reject(err)
      }
      else{
        resolve(data)
      }
   })
 })
} 
readile(lipsum).then((data)=>{
  const uppercaseContent = data.toUpperCase()
 return  appendNewFile('uppertext.txt',uppercaseContent)
}).then(()=>{
  return  appendNewFile('filenames.txt','uppertext.txt'+'\n')
}).then(()=>{
    return readile('uppertext.txt')
}).then((data)=>{
  const lowercaseContent = data.toLowerCase().split('.').join('\n')
  return  appendNewFile('lowercasetext.txt',lowercaseContent)
}).then(()=>{
  return  appendNewFile('filenames.txt','lowercasetext.txt'+'\n')
}).then(()=>{
  return readile('lowercasetext.txt')
}).then((data)=>{
  const sortedData = data.split('\n').sort().join('\n')
  return  appendNewFile('sortedtxt.txt',sortedData)
}).then(()=>{
  return  appendNewFile('filenames.txt','sortedtxt.txt')
}).then(()=>{
  return readile('filenames.txt')
}).then((data)=>{
  let filenames = data.split('\n')
   deleteFile(filenames)
}).catch((err)=>{
  console.log(err)
})
    // Convert the content to uppercase & write to a new file

        const appendNewFile =(filename,data)=>{
          return new Promise((resolve,reject)=>{
            let filepath = path.join(__dirname,filename)
            fs.appendFile(filepath, data, (err) => {
              if (err) {
                reject(err)
              }else{
                resolve()
              }
      
          })

        })
        } 

      const deleteFile = (filenames)=>{
          console.log(filenames)
          filenames.map((filename) => {
            let filepath = path.join(__dirname,filename)
            return fs.unlink(filepath, (err) => {
              if (err) {
                console.error(`Error deleting file '${filename}':`, err)
              } else {
                console.log(`File '${filename}' deleted successfully.`)
              }
            })
          })
      }

    // //Read the new file, convert to lowercase, split into sentences, and write to a new file

    // return new Promise((resolve,reject)=>{
    //   fs.readFile(newFileName, 'utf8', (err, newData) => {
    //       if (err) {
    //         reject(err)
    //       }

    //       const lowercaseContent = newData.toLowerCase()
    //       const sentences = lowercaseContent.split('. ')
    //       const sentenceFileName = 'sentences.txt'

    //       fs.writeFile(sentenceFileName, sentences.join('\n'), (err) => {
    //         if (err) {
    //           reject(err)
    //         }else{
    //           resolve()
    //         }

    //       })

    //     //Read the new files, sort the content, and write it out to a new file
    //     fs.readFile(sentenceFileName, 'utf8', (err, sortedData) => {
    //       if (err) {
    //         console.error('Error :', err)
    //         return
    //       }

    //       const sortedContent = sortedData.split('\n').sort().join('\n')
    //       const sortedFileName = 'sorted.txt'

    //       fs.writeFile(sortedFileName, sortedContent, (err) => {
    //         if (err) {
    //           console.error('Error:', err)
    //           return
    //         }

    //         //Write the filenames to filenames.txt and delete the files simultaneously
    //         const filenames = [newFileName, sentenceFileName, sortedFileName]
    //         const filenamesContent = filenames.join('\n')
    //         const filenamesFileName = 'filenames.txt'

    //         fs.writeFile(filenamesFileName, filenamesContent, (err) => {
    //           if (err) {
    //             console.error('Error:', err)
    //             return
    //           }

    //           console.log('Filenames written to filenames.txt')

              // filenames.map((filename) => {
              //   return fs.unlink(filename, (err) => {
              //     if (err) {
              //       console.error(`Error deleting file '${filename}':`, err)
              //     } else {
              //       console.log(`File '${filename}' deleted successfully.`)
              //     }
              //   })
              //  })
    //         })
    //       })
    //     })
    //   })
    // })
  }

module.exports = tempFile